#!/bin/bash
# Creado por: daniel pabon
# Ultima actualizacion: 2016-07-07
# Una copia actualizada de este script puede ser encontrada en:
# https://github.com/dpabon/bio_comparada/blob/master/siste.sh
# Sugerencias, dudas o comentarios, escriba a: dpabon@openmailbox.org
# Solo para distribuciones basadas en Debian GNU/Linux 64 bits.

#This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Script de instalación programas utiles en sistematica.
# El siguiente script instala los programas:
# TNT 1.5
# POY (MPI) 5.1.1
# PAUP 4.0a147
# RaxML (MPI) 8.2.8
# PhyML (MPI)
# Mr.Bayes (MPI) 3.2.6
# SeaView
# ReadSeq
# Sequence Matrix
# Mesquite
# Geany
# Vim
# Emacs
# Bioperl
# Biopython
# Figtree
# BEAST 1.8.0
# BEAUti
# LogCombiner
# TreeAnnotator
# TreeLogAnalyser
# ExaML
# RevBayes MPI

# Proximamente:
# Tracer
# TreeStat
# JmodelTest
# Prottest

# Por favor revise la totalidad del script antes de ejecutarlo.
# para poder ejecutar este script ud necesita dar permisos de ejecución al mismo:
# sudo chmod +x siste.sh
# luego ejecute el script usando ./siste.sh
# escriba su contraseña de superusuario cuando se le solicite
# si desea solo instalar solo algun programa, dirijase hasta el aparatado del mismo
# y ejecute manualemente las lineas correspondientes.

# Rutina
sudo apt-get update

# Si se quiere ejecutar las versiones para windows de diferentes programas
# lo mejor es instalar playonlinux (wine) y luego dar click en el .exe a ejecutar

#sudo apt-get install playonlinux

# Herramientas de compilación

echo 'Instalando compiladores...'
sudo apt-get install build-essential
sudo apt-get install linux-headers-`uname -r`
sudo apt-get install checkinstall make automake cmake autoconf
echo 'Compiladores instalados'

## Programas de utilidad
sudo apt-get install git
sudo apt-get install unzip

# Editores de codigo utiles
sudo apt-get install geany
sudo apt-get install vim
sudo apt-get install jedit
sudo apt-get install emacs
sudo apt-get install mpich2
# Visualizador de secuencias SeaView
sudo apt-get install seaview

# "Conversor de formato" Readseq
# para obtener informacion sobre el programa teclee man readseq
sudo apt-get install readseq

# Bioperl
#sudo apt-get install bioperl

# Biopython
#sudo apt-get install python-biopython


## Figtree
sudo apt-get install figtree

# Sequence Matrix
# http://gaurav.github.io/taxondna/
echo 'Se creara una carpeta llamada SequenceMatrix en el directorio personal'
read -p 'para guardar el programa. Presione [ENTER] para continuar:'
wget https://github.com/gaurav/taxondna/archive/1.8.tar.gz
tar -xzvf 1.8.tar.gz
cd taxondna-1.8/Release/SequenceMatrix/
sudo mkdir /usr/local/lib/SequenceMatrix/
sudo mv SequenceMatrix.jar /usr/local/lib/SequenceMatrix/
cd ../../..
rm -rf taxondna-1.8/
rm 1.8.tar.gz
touch sequencematrix
echo '#!/bin/bash' >> sequencematrix
echo 'java -jar /usr/local/lib/SequenceMatrix/SequenceMatrix.jar' >> sequencematrix
sudo chmod +x sequencematrix
sudo mv sequencematrix /usr/local/bin/
echo 'SequenceMatrix instalado.'
echo 'para ejecutar SequenceMatrix escriba en una terminal sequencematrix'
echo '.....................'
# Mesquite
echo 'Se creara una carpeta llamada Mesquite en el directorio personal'
read -p 'para guardar el programa. Presione [ENTER] para continuar:'
echo 'Instalando Mesquite...'
wget https://github.com/MesquiteProject/MesquiteCore/releases/download/v3.10-build-765/Mesquite310-Linux.tgz
tar -xvzf Mesquite310-Linux.tgz
sudo mv Mesquite_Folder/ /usr/local/lib/Mesquite/
rm -rf Mesquite_Folder/
sudo chmod +x /usr/local/lib/Mesquite/mesquite.sh
rm Mesquite310-Linux.tgz
touch mesquite
echo '#!/bin/bash' >> mesquite
echo 'cd /usr/local/lib/Mesquite/' >> mesquite
echo './mesquite.sh' >> mesquite
sudo chmod +x mesquite
sudo mv mesquite /usr/local/bin/
echo 'Mesquite instalado'
echo 'para ejecutar mesquite escriba en una terminal mesquite'
echo '...................'


## TNT
echo 'Instalando TNT ...'
wget http://www.lillo.org.ar/phylogeny/tnt/tnt64-no-tax-limit.zip
mkdir tnt
unzip tnt64-no-tax-limit.zip -d tnt
cd tnt/
sudo chmod +x tnt
sudo mv tnt /usr/local/bin/
cd ..
rm -rf tnt/
rm tnt64-no-tax-limit.zip
echo 'TNT instalado'
echo 'para ejecutar TNT escriba en una terminal tnt'

## POY -version paralelo
echo 'Instalando POY'
wget http://www.amnh.org/content/download/67796/1174582/version/3/file/poy_5.1.1-src.tar.gz
sudo apt-get install mpich2
sudo apt-get install libz-dev ocaml ocaml-native-compilers camlp4-extra opam camlp4-extra ncurses-base libncurses5
tar -xzvf poy_5.1.1-src.tar.gz
rm poy_5.1.1-src.tar.gz
cd poy_5.1.1/src
sudo ./configure --enable-interface=flat --enable-mpi CC=mpicc --enable-long-sequences=Enable --enable-likelihood=Enable
sudo make
sudo make install
cd ../..
sudo rm -rf poy_5.1.1
echo 'POY instalado'
echo '......................'
## RaxML compiled in parallel
echo 'Instalando RaxML...'
git clone https://github.com/stamatak/standard-RAxML.git
cd standard-RAxML
sudo make -f Makefile.MPI.gcc
sudo rm *.o
sudo mv raxmlHPC-MPI /usr/local/bin/
cd ..
sudo rm -rf standard-RAxML
echo 'RaxML instalado'
echo '.....................'
## PAUP
echo 'Instalando PAUP'
sudo apt-get install liblapack3
wget http://people.sc.fsu.edu/~dswofford/paup_test/paup4a147_ubuntu64.gz
gzip -d paup4a147_ubuntu64.gz
rm paup4a147_ubuntu64.gz
mv paup4a147_ubuntu64 paup
sudo chmod +x paup
sudo mv paup /usr/local/bin/
echo 'PAUP instalado'
echo '...................'

## MrBayes compiled in parallel
read -p 'Instalando Mr. Bayes \n
Presione [ENTER] para continuar'
wget http://downloads.sourceforge.net/project/mrbayes/mrbayes/3.2.6/mrbayes-3.2.6.tar.gz?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmrbayes%2Ffiles%2Fmrbayes%2F&ts=1459893883&use_mirror=netix
mv mrbayes-3.2.6.tar.gz\?r\=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmrbayes%2Ffiles%2Fmrbayes%2F mrbayes-3.2.6.tar.gz
tar -xzvf mrbayes-3.2.6.tar.gz
cd mrbayes-3.2.6/src
sudo autoconf
sudo ./configure --with-beagle=no --enable-mpi=yes
sudo make
sudo make install
cd ../..
sudo rm -rf mrbayes-3.2.6 mrbayes-3.2.6.tar.gz
echo 'MrBayes instalado'

## BEAST
echo 'Instalando BEAST'
wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/beast-mcmc/BEASTv1.8.0.tgz
tar -xvzf BEASTv1.8.0.tgz
rm BEASTv1.8.0.tgz
sudo mv BEASTv1.8.0/lib/* /usr/local/lib/
sudo mv BEASTv1.8.0/bin/* /usr/local/bin/
sudo rm -rf BEASTv1.8.0
echo 'BEAST instalado'
## PhyML compiled in parallel
sudo apt-get update
sudo apt-get install libtool
git clone https://github.com/stephaneguindon/phyml.git
cd phyml
sudo ./autogen.sh
sudo ./configure --enable-mpi
sudo make
sudo make install
cd ..
sudo rm -rf phyml

## ExaML
echo 'Instalando ExaML'
git clone https://github.com/stamatak/ExaML
cd ExaML/parser/
make -f Makefile.SSE3.gcc
sudo mv parse-examl /usr/local/bin/
cd ../examl
make -f Makefile.SSE3.gcc
sudo mv examl /usr/local/bin/
cd ..
sudo rm -rf ExaML
echo 'ExaML instalado'

## RevBayes
echo 'Instalando revBayes'
git clone https://github.com/revbayes/revbayes
cd revbayes/projects/cmake
./build.sh -mpi true
sudo mv rb-mpi /usr/local/bin/
cd ../../..
sudo rm -rf revbayes
echo 'revBayes instalado'
